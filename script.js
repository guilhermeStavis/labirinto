const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

/* -------------- CRIAÇÃO DO LABIRINTO --------------  */

for (let i = 0; i < map.length; i++) {
    let outerCell = document.createElement('div')
    outerCell.className = 'line'
    outerCell.id = `line${i+1}`
    document.getElementById('game').appendChild(outerCell)

    for (let j = 0; j < map[i].length; j ++) {
        let cell = document.createElement('div')
        if (map[i][j] === 'W') {
            cell.className = 'wall'
        } else if(map[i][j] === ' ' || map[i][j] === 'S' || map[i][j] === 'F') {
            cell.className = 'floor'
        }
        cell.classList.add(`column${j+1}`)
        
        if (map[i][j] === 'S') {
            cell.id = 'start'
            let img = document.createElement('img')
            img.id = 'player'
            img.src = './src/peao.png'
            cell.appendChild(img)
        } else if (map[i][j] === 'F') {
            cell.id = 'finish'
        }
        document.getElementById(`line${i+1}`).appendChild(cell)
    }
}

/* -------------- SISTEMA DE MOVIMENTAÇÃO --------------  */

let player = document.getElementById('player')
let celula = player.parentElement
let linha = celula.parentElement

document.addEventListener('keydown', (event) => {
    const keyName = event.key
    console.log(keyName)

    if (keyName === 'ArrowUp') {
        if (linha.previousSibling !== null) {
            linha = linha.previousSibling
        }
        const colunaAtual = celula.classList[1]
        const filhos = linha.childNodes
        
        for (let i = 0; i < filhos.length; i++) {
            if (filhos[i].classList.contains(colunaAtual)){
                if (filhos[i].classList.contains('floor')) {
                    celula = filhos[i]
                } else {
                    linha = linha.nextSibling
                }
            }
        }
        celula.appendChild(player)
        
    } else if (keyName === 'ArrowLeft') {
        if (celula.previousSibling === null) {
            alert('Movimento Inválido')
        } else if (celula.previousSibling.classList.contains('floor')){
            celula = celula.previousSibling
            celula.appendChild(player)
        }
    } else if (keyName === 'ArrowDown') {
        if (linha.nextSibling !== null) {
            linha = linha.nextSibling
        }
        const colunaAtual = celula.classList[1]
        const filhos = linha.childNodes
        
        for (let i = 0; i < filhos.length; i++) {
            if (filhos[i].classList.contains(colunaAtual)){
                if (filhos[i].classList.contains('floor')) {
                    celula = filhos[i]
                } else {
                    linha = linha.previousSibling
                }
            }
            
        }
        celula.appendChild(player)

    } else if (keyName === 'ArrowRight') {
        if (celula.nextSibling === null) {
            alert('Movimento Inválido')
        } else if (celula.nextSibling.classList.contains('floor')){
            celula = celula.nextSibling
            celula.appendChild(player)
        }
    }
    checkWin()
})

/* -------------- MENSAGEM DE VITÓRIA -------------- */
let divWin = document.createElement('div')
const checkWin = () => {
    if (player.parentElement.id === 'finish') {
        divWin.id = 'divWin'
        divWin.innerHTML = '<p>Parabéns! Você saiu do labirinto!</p>'
        document.body.appendChild(divWin)
        
        let buttonReset = document.createElement('button')
        buttonReset.innerText = 'ok'
        buttonReset.className = 'reset'
        buttonReset.addEventListener('click', reset)
        divWin.appendChild(buttonReset)
    }
}

/* -------------- RESETANDO O GAME -------------- */

const reset = () => {
    document.getElementById('start').appendChild(player)
    celula = player.parentElement
    linha = celula.parentElement
    divWin.remove()
}




 



